package br.com.apifood.controllers;

import br.com.apifood.enums.TipoProdutoEnum;
import br.com.apifood.models.Produto;
import br.com.apifood.security.JWTUtil;
import br.com.apifood.services.LoginUsuarioService;
import br.com.apifood.services.ProdutoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.Arrays;


@WebMvcTest(ProdutoController.class)
public class ProdutoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private JWTUtil jwtUtil;

    @MockBean
    private LoginUsuarioService loginUsuarioService;

    @MockBean
    private ProdutoService produtoService;

    Produto produto;

    @BeforeEach
    public void setUp() {
        produto = new Produto();
        produto.setId(1);
        produto.setDescricao("Descricao Teste");
        produto.setEstaDisponivel(true);
        produto.setNome("Teste");
        produto.setPreco(10);
        produto.setTipoProduto(TipoProdutoEnum.COMIDA);
    }

    @Test
    public void testarConsultarProdutoPorID() throws Exception {
        Mockito.when(produtoService.consultarProdutoPorID(Mockito.anyInt())).thenReturn(produto);

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeLead = mapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.get("/produtos/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeLead))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Teste")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.preco", CoreMatchers.equalTo(10.0)));

    }

    @Test
    public void testarConsultarProdutoPorIdQueNaoExiste() throws Exception {
        Mockito.when(produtoService.consultarProdutoPorID(Mockito.anyInt())).thenThrow(RuntimeException.class);

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeLead = mapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.get("/produtos/2")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeLead))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());

    }

    @Test
    public void testarConsultarProdutoPorNome() throws Exception {
        Iterable<Produto> produtoIterable = Arrays.asList(produto);
        Mockito.when(produtoService.consultarProdutoPorNome(Mockito.anyString())).thenReturn(produtoIterable);

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeLead = mapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.get("/produtos?nomeProduto=Teste")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeLead))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1));
    }

    @Test
    public void testarConsultarProdutoPorNome2() throws Exception {
        Produto produto2 = new Produto();
        produto2.setId(2);
        produto2.setDescricao("Descricao Teste");
        produto2.setEstaDisponivel(true);
        produto2.setNome("Teste");
        produto2.setPreco(10);
        produto2.setTipoProduto(TipoProdutoEnum.COMIDA);
        Iterable<Produto> produtoIterable = Arrays.asList(produto, produto2);

        Mockito.when(produtoService.consultarProdutoPorNome(Mockito.anyString())).thenReturn(produtoIterable);

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeLead = mapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.get("/produtos?nomeProduto=Teste")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeLead))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(2));
    }

    @Test
    public void testarSeProdutoEstaDisponivel() throws Exception{
        Mockito.when(produtoService.consultarSeProdutoEstaDisponivel(Mockito.anyInt())).thenReturn(true);;

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeLead = mapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.get("/produtos/1/status")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeLead))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }
}
