package br.com.apifood.services;

import br.com.apifood.enums.TipoCidadeEnum;
import br.com.apifood.enums.TipoProdutoEnum;
import br.com.apifood.models.Produto;
import br.com.apifood.models.Restaurante;
import br.com.apifood.repositories.ProdutoRepository;
import br.com.apifood.repositories.RestauranteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    @Autowired
    private RestauranteService restauranteService;

    @Autowired
    private RestauranteRepository restauranteRepository;

    public Produto criarProduto(Produto produto, int id) {
        Restaurante restaurante = restauranteService.buscarPorId(id);
        List<Produto> produtos = restaurante.getProdutos();
        produtos.add(produto);
        restauranteService.atualizarRestaurante(id, restaurante);
        return produto;
    }

    public Iterable<Produto> consultarTodosProdutos() {
        Iterable<Produto> produtoIterable = produtoRepository.findAll();
        return produtoIterable;
    }

    public Produto consultarProdutoPorID(int id) {
        Optional<Produto> optionalProduto = produtoRepository.findById(id);
        if (optionalProduto.isPresent()) {
            return optionalProduto.get();
        }
        throw new RuntimeException("Produto não encontrado");
    }

    public Iterable<Produto> consultarProdutoPorNome(String nome) {
        List<Produto> produtoIterable = produtoRepository.findAllByNome(nome);
        if (!produtoIterable.isEmpty()) {
            return produtoIterable;
        } else {
            produtoIterable = produtoRepository.findAllByNomeContaining(nome);
            if (!produtoIterable.isEmpty()) {
                return produtoIterable;
            }
        }
        throw new RuntimeException("Não existem produtos com esse nome");
    }

    public Iterable<Produto> consultarProdutoPorTipoDeProduto(TipoProdutoEnum produtoEnum) {
        List<Produto> produtoIterable = produtoRepository.findAllByTipoProduto(produtoEnum);
        if (!produtoIterable.isEmpty()) {
            return produtoIterable;
        }
        throw new RuntimeException("Não existem produtos desse tipo");
    }

    public Iterable<Restaurante> consultarProdutosPorCidade(TipoCidadeEnum cidadeEnum) {
        Iterable<Restaurante> restauranteIterable = restauranteRepository.findAllByCidade(cidadeEnum);
        return restauranteIterable;
    }

    public boolean consultarSeProdutoEstaDisponivel(int id) {
        Optional<Produto> produto = produtoRepository.findById(id);
        if (produto.isPresent()) {
            if (produto.get().isEstaDisponivel()) {
                return true;
            } else {
                return false;
            }
        }
        throw new RuntimeException("Produto não encontrado");
    }

    public Produto atualizarProduto(int idProduto, int idRestaurante, Produto produto) {
        Optional<Restaurante> restaurante = restauranteRepository.findById(idRestaurante);
        if (restaurante.isPresent()) {
            List<Produto> produtos = restaurante.get().getProdutos();
            for (Produto p : produtos) {
                if (p.getId() == idProduto) {
                    produto.setId(idProduto);
                    Produto produtoObjeto = produtoRepository.save(produto);
                    return produtoObjeto;
                }
            }
            throw new RuntimeException("Produto não encontrado");
        }
        throw new RuntimeException("Restaurante não encontrado");
    }


    public boolean deletarProduto(int id){
        if(produtoRepository.existsById(id)){
            produtoRepository.deleteById(id);
            return true;
        }else{
            throw new RuntimeException("Produto não encontrado");
        }
    }

}
