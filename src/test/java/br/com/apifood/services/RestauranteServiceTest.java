package br.com.apifood.services;

import br.com.apifood.enums.TipoCategoriaEnum;
import br.com.apifood.enums.TipoCidadeEnum;
import br.com.apifood.enums.TipoProdutoEnum;
import br.com.apifood.models.Produto;
import br.com.apifood.models.Restaurante;
import br.com.apifood.repositories.RestauranteRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class RestauranteServiceTest {

    @MockBean
    private RestauranteRepository restauranteRepository;

    @Autowired
    private RestauranteService restauranteService;

    Restaurante restaurante;
    Produto produto;
    List<Produto> produtos;

    @BeforeEach
    public void setUp(){
        restaurante = new Restaurante();
        restaurante.setId(1);
        restaurante.setNome("Hamburgueria One");
        restaurante.setTelefone("998688447");
        restaurante.setCategoria(TipoCategoriaEnum.HAMBURGUERIA);
        restaurante.setCidade(TipoCidadeEnum.SAO_PAULO);

        produto = new Produto();
        produto.setId(1);
        produto.setNome("Hamburguer da Casa");
        produto.setDescricao("Hamburgão");
        produto.setPreco(30.00);
        produto.setTipoProduto(TipoProdutoEnum.COMIDA);
        produto.setEstaDisponivel(true);

        produtos = new ArrayList<>();
        produtos.add(produto);

        restaurante.setProdutos(produtos);
    }

    @Test
    public void testarBuscarPorId(){
        Optional<Restaurante> restauranteOptional = Optional.of(restaurante);
        Mockito.when(restauranteRepository.findById(restaurante.getId())).thenReturn(restauranteOptional);

        Restaurante restauranteResposta = restauranteService.buscarPorId(restaurante.getId());

        Assertions.assertEquals(restauranteOptional.get(), restauranteResposta);
    }

    @Test
    public void testarBuscarPorTodosOsRestaurantes(){
        Iterable<Restaurante> restaurantes = Arrays.asList(restaurante);
        Mockito.when(restauranteRepository.findAll()).thenReturn(restaurantes);

        Iterable<Restaurante> restaurantesIterable = restauranteService.buscarTodosOsRestaurantes();

        Assertions.assertEquals(restaurantes, restaurantesIterable);
    }

    @Test
    public void testarBuscarTodosPorCidade(){
        Iterable<Restaurante> restaurantes = Arrays.asList(restaurante);
        Mockito.when(restauranteRepository.findAllByCidade(restaurante.getCidade())).thenReturn(restaurantes);

        Iterable<Restaurante> restaurantesIterable = restauranteService.buscarTodosPorCidade(restaurante.getCidade());

        Assertions.assertEquals(restaurantes, restaurantesIterable);
    }

    @Test
    public void testarBuscarTodosPorCategoria(){
        Iterable<Restaurante> restaurantes = Arrays.asList(restaurante);
        Mockito.when(restauranteRepository.findAllByCategoria(restaurante.getCategoria())).thenReturn(restaurantes);

        Iterable<Restaurante> restaurantesIterable = restauranteService.buscarTodosPorTipoDeRestaurante(restaurante.getCategoria());

        Assertions.assertEquals(restaurantes, restaurantesIterable);
    }

    @Test
    public void testarBuscarTodosPorNome(){
        Iterable<Restaurante> restaurantes = Arrays.asList(restaurante);
        Mockito.when(restauranteRepository.findAllByNomeContaining(restaurante.getNome())).thenReturn(restaurantes);

        Iterable<Restaurante> restaurantesIterable = restauranteService.buscarTodosPorNome(restaurante.getNome());

        Assertions.assertEquals(restaurantes, restaurantesIterable);
    }

    @Test
    public void testarAtualizarRestaurante(){
        Mockito.when(restauranteRepository.existsById(Mockito.anyInt())).thenReturn(true);
        Mockito.when(restauranteRepository.save(restaurante)).thenReturn(restaurante);

        Restaurante restauranteObjeto = restauranteService.atualizarRestaurante(1, restaurante);

        Assertions.assertEquals(restaurante, restauranteObjeto);
    }
    @Test
    public void testarDeletarRestaurante(){
        Mockito.when(restauranteRepository.existsById(Mockito.anyInt())).thenReturn(true);

        restauranteService.deletarRestaurante(1);

        //Aqui está apenas verificando se houve uma chamada do método de deletar
        Mockito.verify(restauranteRepository, Mockito.times(1)).deleteById(Mockito.anyInt());
    }

    @Test
    public void testarSalvarRestaurante(){
        Restaurante restauranteSalvar = new Restaurante();
        restauranteSalvar.setId(2);
        restauranteSalvar.setNome("Japones One");
        restauranteSalvar.setTelefone("998688447");
        restauranteSalvar.setCategoria(TipoCategoriaEnum.JAPONESA);
        restauranteSalvar.setCidade(TipoCidadeEnum.VARGEM_GRANDE_PAULISTA);

        Mockito.when(restauranteRepository.save(restauranteSalvar)).thenReturn(restauranteSalvar);

        Restaurante restauranteObjeto = restauranteService.salvarRestaurante(restauranteSalvar);

        Assertions.assertEquals(restauranteSalvar, restauranteObjeto);
    }
}