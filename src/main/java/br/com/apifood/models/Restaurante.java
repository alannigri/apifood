package br.com.apifood.models;

import br.com.apifood.enums.TipoCategoriaEnum;
import br.com.apifood.enums.TipoCidadeEnum;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
public class Restaurante {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    private String nome;

    @NotNull
    private String telefone;

    @NotNull
    private TipoCidadeEnum cidade;

    @NotNull
    private TipoCategoriaEnum categoria;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Produto> produtos;

    public Restaurante() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }

    public TipoCidadeEnum getCidade() {
        return cidade;
    }

    public void setCidade(TipoCidadeEnum cidade) {
        this.cidade = cidade;
    }

    public TipoCategoriaEnum getCategoria() {
        return categoria;
    }

    public void setCategoria(TipoCategoriaEnum categoria) {
        this.categoria = categoria;
    }
}