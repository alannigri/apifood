package br.com.apifood.services;

import br.com.apifood.enums.TipoCategoriaEnum;
import br.com.apifood.enums.TipoCidadeEnum;
import br.com.apifood.enums.TipoProdutoEnum;
import br.com.apifood.models.Cliente;
import br.com.apifood.models.Produto;
import br.com.apifood.models.Restaurante;
import br.com.apifood.repositories.ClienteRepository;
import br.com.apifood.repositories.ProdutoRepository;
import br.com.apifood.repositories.RestauranteRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class ProdutoServiceTest {

    @MockBean
    private ProdutoRepository produtoRepository;

    @MockBean
    private RestauranteRepository restauranteRepository;

    @MockBean
    private RestauranteService restauranteService;

    @Autowired
    private ProdutoService produtoService;

    Produto produto;
    List<Produto> produtos;

    @BeforeEach
    public void setUp(){
        produto = new Produto();
        produto.setId(1);
        produto.setNome("Hamburguer da Casa");
        produto.setDescricao("Hamburgão");
        produto.setPreco(30.00);
        produto.setTipoProduto(TipoProdutoEnum.COMIDA);
        produto.setEstaDisponivel(true);
    }

    @Test
    public void testarBuscarPorId(){
        Optional<Produto> produtoOptional = Optional.of(produto);
        Mockito.when(produtoRepository.findById(produto.getId())).thenReturn(produtoOptional);

        Produto produtoResposta = produtoService.consultarProdutoPorID(produto.getId());

        Assertions.assertEquals(produtoOptional.get(), produtoResposta);
    }

    @Test
    public void testarBuscarPorTodosOsProdutos(){
        Iterable<Produto> produtos = Arrays.asList(produto);
        Mockito.when(produtoRepository.findAll()).thenReturn(produtos);

        Iterable<Produto> produtosIterable = produtoService.consultarTodosProdutos();

        Assertions.assertEquals(produtos, produtosIterable);
    }

    @Test
    public void testarBuscarTodosPorTipoDeProduto(){
        List<Produto> produtos = Arrays.asList(produto);
        Mockito.when(produtoRepository.findAllByTipoProduto(produto.getTipoProduto())).thenReturn(produtos);

        Iterable<Produto> produtosIterable = produtoService.consultarProdutoPorTipoDeProduto(produto.getTipoProduto());

        Assertions.assertEquals(produtos, produtosIterable);
    }

    @Test
    public void testarBuscarTodosPornome(){
        List<Produto> produtos = Arrays.asList(produto);
        Mockito.when(produtoRepository.findAllByNome(produto.getNome())).thenReturn(produtos);

        Iterable<Produto> produtosIterable = produtoService.consultarProdutoPorNome(produto.getNome());

        Assertions.assertEquals(produtos, produtosIterable);
    }

    @Test
    public void testarPesquisarSeProdutoEstaDisponivel(){
        Optional<Produto> produtoOptional = Optional.of(produto);
        Mockito.when(produtoRepository.existsById(Mockito.anyInt())).thenReturn(true);
        Mockito.when(produtoRepository.findById(produto.getId())).thenReturn(produtoOptional);

        Boolean estaDisponivel = produtoService.consultarSeProdutoEstaDisponivel(produto.getId());

        Assertions.assertEquals(true, estaDisponivel);
    }

    @Test
    public void testarDeletarProduto(){
        Mockito.when(produtoRepository.existsById(Mockito.anyInt())).thenReturn(true);

        produtoService.deletarProduto(1);

        //Aqui está apenas verificando se houve uma chamada do método de deletar
        Mockito.verify(produtoRepository, Mockito.times(1)).deleteById(Mockito.anyInt());
    }

    @Test
    public void testarSalvarProduto(){
        Restaurante restaurante = new Restaurante();
        restaurante.setId(2);
        restaurante.setNome("Japones One");
        restaurante.setTelefone("998688447");
        restaurante.setCategoria(TipoCategoriaEnum.JAPONESA);
        restaurante.setCidade(TipoCidadeEnum.VARGEM_GRANDE_PAULISTA);

        produtos = new ArrayList<>();
        produtos.add(produto);
        restaurante.setProdutos(produtos);

        Produto produtoSalvar = new Produto();
        produtoSalvar = new Produto();
        produtoSalvar.setId(2);
        produtoSalvar.setNome("JAPA");
        produtoSalvar.setDescricao("SUSHI");
        produtoSalvar.setPreco(50.00);
        produtoSalvar.setTipoProduto(TipoProdutoEnum.COMIDA);
        produtoSalvar.setEstaDisponivel(true);

        Mockito.when(restauranteService.buscarPorId(restaurante.getId())).thenReturn(restaurante);
        Mockito.when(produtoRepository.save(produtoSalvar)).thenReturn(produtoSalvar);

        Produto produtoObjeto = produtoService.criarProduto(produtoSalvar, restaurante.getId());

        Assertions.assertEquals(produtoSalvar, produtoObjeto);
    }
}
