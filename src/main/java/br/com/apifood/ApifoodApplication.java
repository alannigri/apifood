package br.com.apifood;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApifoodApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApifoodApplication.class, args);
	}

}
