package br.com.apifood.services;

import br.com.apifood.enums.TipoCidadeEnum;
import br.com.apifood.models.Cliente;
import br.com.apifood.repositories.ClienteRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
public class ClienteServiceTest {

    @MockBean
    private ClienteRepository clienteRepository;

    @Autowired
    private ClienteService clienteService;

    Cliente cliente;

    @BeforeEach
    public void setUp(){
        cliente = new Cliente();
        cliente.setId(1);
        cliente.setNome("Marcelo");
        cliente.setTelefone("998388685");
        cliente.setTipoCidade(TipoCidadeEnum.SAO_PAULO);
    }

    @Test
    public void testarBuscarPorId(){
        Optional<Cliente> clienteOptional = Optional.of(cliente);
        Mockito.when(clienteRepository.findById(cliente.getId())).thenReturn(clienteOptional);

        Cliente clienteResposta = clienteService.buscarPorId(cliente.getId());

        Assertions.assertEquals(clienteOptional.get(), clienteResposta);
    }

    @Test
    public void testarBuscarPorTodosOsCLientes(){
        Iterable<Cliente> clientes = Arrays.asList(cliente);
        Mockito.when(clienteRepository.findAll()).thenReturn(clientes);

        Iterable<Cliente> clientesIterable = clienteService.buscarTodosOsClientes();

        Assertions.assertEquals(clientes, clientesIterable);
    }

    @Test
    public void testarBuscarTodosPorCidade(){
        Iterable<Cliente> clientes = Arrays.asList(cliente);
        Mockito.when(clienteRepository.findAllByTipoCidade(cliente.getTipoCidade())).thenReturn(clientes);

        Iterable<Cliente> clientesIterable = clienteService.buscarPorTodosOsTipoCidade(cliente.getTipoCidade());

        Assertions.assertEquals(clientes, clientesIterable);
    }

    @Test
    public void testarAtualizarCliente(){
        Mockito.when(clienteRepository.existsById(Mockito.anyInt())).thenReturn(true);
        Mockito.when(clienteRepository.save(cliente)).thenReturn(cliente);

        Cliente clienteObjeto = clienteService.atualizarCliente(1, cliente);

        Assertions.assertEquals(cliente, clienteObjeto);
    }
    @Test
    public void testarDeletarCLiente(){
        Mockito.when(clienteRepository.existsById(Mockito.anyInt())).thenReturn(true);

        clienteService.deletarCliente(1);

        //Aqui está apenas verificando se houve uma chamada do método de deletar
        Mockito.verify(clienteRepository, Mockito.times(1)).deleteById(Mockito.anyInt());
    }

    @Test
    public void testarSalvarRestaurante(){
        Cliente clienteSalvar = new Cliente();
        clienteSalvar.setId(2);
        clienteSalvar.setNome("Marcelo Luiz");
        clienteSalvar.setTelefone("998388685");
        clienteSalvar.setTipoCidade(TipoCidadeEnum.CAMPINAS);

        Mockito.when(clienteRepository.save(clienteSalvar)).thenReturn(clienteSalvar);

        Cliente clienteObjeto = clienteService.salvarCliente(clienteSalvar);

        Assertions.assertEquals(clienteSalvar, clienteObjeto);
    }
}
