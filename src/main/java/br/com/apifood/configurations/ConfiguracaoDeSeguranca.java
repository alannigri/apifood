package br.com.apifood.configurations;

import br.com.apifood.security.FiltroAutorizacao;
import br.com.apifood.security.FiltroDeAutenticacao;
import br.com.apifood.security.JWTUtil;
import br.com.apifood.services.LoginUsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
@EnableWebSecurity
public class ConfiguracaoDeSeguranca extends WebSecurityConfigurerAdapter {

    @Autowired
    private JWTUtil jwtUtil;

    @Autowired
    private LoginUsuarioService loginUsuarioService;

    private static final String[] PUBLIC_MATCHERS_GET = {
            "/restaurantes",
            "/restaurantes/*",
            "/produtos",
            "/produtos/*",
            "/produtos/*/*"
    };

    private static final String[] PUBLIC_MATCHERS_POST = {
//            "/clientes",
//            "/restaurantes",
            "/usuario",
            "/login"
//            "/produtos",
//            "/restaurantes/*/produtos"
    };

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.cors();

        http.authorizeRequests()
                .antMatchers(HttpMethod.POST, PUBLIC_MATCHERS_POST).permitAll()
                .antMatchers(HttpMethod.GET, PUBLIC_MATCHERS_GET).permitAll()
                .anyRequest().authenticated();

        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        //filtros JWT
        http.addFilter(new FiltroDeAutenticacao(authenticationManager(), jwtUtil));
        http.addFilter(new FiltroAutorizacao(authenticationManager(),jwtUtil,loginUsuarioService));
    }

    @Bean
    CorsConfigurationSource configuracaoDeCors(){
        final UrlBasedCorsConfigurationSource cors = new UrlBasedCorsConfigurationSource();
        cors.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());
        return cors;
    }

    @Bean
    BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(loginUsuarioService).passwordEncoder(bCryptPasswordEncoder());
    }
}
