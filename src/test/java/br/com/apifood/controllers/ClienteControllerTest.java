package br.com.apifood.controllers;

import br.com.apifood.configurations.ConfiguracaoDeSeguranca;
import br.com.apifood.enums.TipoCidadeEnum;
import br.com.apifood.models.Cliente;
import br.com.apifood.security.JWTUtil;
import br.com.apifood.services.ClienteService;
import br.com.apifood.services.LoginUsuarioService;
import br.com.apifood.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import sun.net.www.http.HttpClient;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@WebMvcTest(ClienteController.class)
public class ClienteControllerTest {

    @MockBean
    private ClienteService clienteService;

    @MockBean
    private LoginUsuarioService loginUsuarioService;

    @MockBean
    private JWTUtil jwtUtil;


    @Autowired
    private MockMvc mockMvc;

    Cliente cliente;

    @BeforeEach
    public void setUp(){
        cliente = new Cliente();
        cliente.setNome("Diego");
        cliente.setTelefone("988884444");
        cliente.setTipoCidade(TipoCidadeEnum.SAO_PAULO);
    }

    @Test
    public void testarNegativoCadastrarClienteValidoSemToken() throws Exception{

        Mockito.when(clienteService.salvarCliente(Mockito.any(Cliente.class))).then(clienteObjeto -> {
            cliente.setId(1);
            return cliente;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeCliente = mapper.writeValueAsString(cliente);

        mockMvc.perform(MockMvcRequestBuilders.post("/clientes")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeCliente))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }
}
