package br.com.apifood.repositories;

import br.com.apifood.enums.TipoCategoriaEnum;
import br.com.apifood.enums.TipoCidadeEnum;
import br.com.apifood.models.Restaurante;
import org.springframework.data.repository.CrudRepository;

public interface RestauranteRepository extends CrudRepository<Restaurante, Integer> {
    Iterable<Restaurante> findAllByCidade(TipoCidadeEnum tipoCidadeEnum);
    Iterable<Restaurante> findAllByCategoria(TipoCategoriaEnum tipoCategoria);
    Iterable<Restaurante> findAllByNomeContaining(String nome);
}
