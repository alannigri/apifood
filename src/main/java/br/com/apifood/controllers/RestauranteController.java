package br.com.apifood.controllers;

import br.com.apifood.enums.TipoCategoriaEnum;
import br.com.apifood.enums.TipoCidadeEnum;
import br.com.apifood.models.Produto;
import br.com.apifood.models.Restaurante;
import br.com.apifood.services.ProdutoService;
import br.com.apifood.services.RestauranteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/restaurantes")
public class RestauranteController {

    @Autowired
    private RestauranteService restauranteService;

    @Autowired
    private ProdutoService produtoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Restaurante registrarRestaurante(@RequestBody @Valid Restaurante restaurante){
        Restaurante restauranteObjeto = restauranteService.salvarRestaurante(restaurante);
        return restauranteObjeto;
    }

    @PostMapping("/{id}/produtos")
    @ResponseStatus(HttpStatus.CREATED)
    public Restaurante criarProdutoDoRestaurante(@PathVariable (name = "id") Integer id,
                                                 @RequestBody @Valid Produto produto){
        produtoService.criarProduto(produto, id);
        Restaurante restaurante = restauranteService.buscarPorId(id);
        return restaurante;
    }

    @PutMapping("/{idRestaurante}/produtos/{idProduto}")
    @ResponseStatus(HttpStatus.CREATED)
    public Restaurante atualizaProdutoDoRestaurante(@PathVariable (name = "idRestaurante") Integer idRestaurante,
                                                    @PathVariable (name = "idProduto") Integer idProduto,
                                                 @RequestBody @Valid Produto produto){

     try {
         produtoService.atualizarProduto(idProduto, idRestaurante, produto);

         Restaurante restaurante = restauranteService.buscarPorId(idRestaurante);
         return restaurante;
     } catch (Exception e){
         throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
     }
    }

    @PutMapping("/{id}")
    public Restaurante atualizarRestaurante(@PathVariable(name = "id") int id, @RequestBody Restaurante restaurante){
        try{
            Restaurante restauranteObjeto = restauranteService.atualizarRestaurante(id, restaurante);
            return restauranteObjeto;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarRestaurante(@PathVariable int id){
        try{
            restauranteService.deletarRestaurante(id);
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Restaurante buscarPorId(@PathVariable(name = "id") int id){
        try{
            Restaurante restaurante = restauranteService.buscarPorId(id);
            return restaurante;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @GetMapping
    public  Iterable<Restaurante> exibirTodos(@RequestParam(name = "categoria", required = false) TipoCategoriaEnum tipoCategoria,
                                             @RequestParam(name = "cidade", required = false) TipoCidadeEnum cidade,
                                             @RequestParam(name = "nome", required = false) String nome){
        if(tipoCategoria != null){
            Iterable<Restaurante> restaurantes = restauranteService.buscarTodosPorTipoDeRestaurante(tipoCategoria);
            return restaurantes;
        }
        if(cidade != null){
            Iterable<Restaurante> restaurantes = restauranteService.buscarTodosPorCidade(cidade);
            return restaurantes;
        }
        if(nome != null){
            Iterable<Restaurante> restaurantes = restauranteService.buscarTodosPorNome(nome);
            return restaurantes;
        }
        Iterable<Restaurante> restaurantes = restauranteService.buscarTodosOsRestaurantes();
        return restaurantes;
    }
}