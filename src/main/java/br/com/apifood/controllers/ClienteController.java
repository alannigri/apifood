package br.com.apifood.controllers;

import br.com.apifood.enums.TipoCidadeEnum;
import br.com.apifood.models.Cliente;
import br.com.apifood.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/clientes")
public class ClienteController {

    @Autowired
    ClienteService clienteService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Cliente cadastrarCliente(@RequestBody @Valid Cliente cliente){
        Cliente clienteObjeto = clienteService.salvarCliente(cliente);
        return clienteObjeto;
    }

    @PutMapping("/{id}")
    public Cliente atualizarCliente(@PathVariable(name = "id") int id, @RequestBody @Valid Cliente cliente){
        Cliente clienteObjeto = clienteService.atualizarCliente(id, cliente);
        return clienteObjeto;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deletarCliente(@PathVariable int id){
        try{
            clienteService.deletarCliente(id);
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }


    @GetMapping
    public Iterable<Cliente> exibirTodos(@RequestParam(name = "tipoDeCidade", required = false) TipoCidadeEnum tipoCidade){
        Iterable<Cliente> clientes;
        if(tipoCidade != null){
            clientes = clienteService.buscarPorTodosOsTipoCidade(tipoCidade);
            return clientes;
        }
        clientes = clienteService.buscarTodosOsClientes();
        return clientes;
    }

    @GetMapping("/{id}")
    public Cliente bucarPorId(@PathVariable(name = "id") int id){
        try{
            Cliente cliente = clienteService.buscarPorId(id);
            return cliente;
        }catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

}
