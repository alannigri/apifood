package br.com.apifood.repositories;

import br.com.apifood.enums.TipoCidadeEnum;
import br.com.apifood.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {

    Iterable<Cliente> findAllByTipoCidade(TipoCidadeEnum tipoCidade);
}

