package br.com.apifood.models;

import br.com.apifood.enums.TipoCidadeEnum;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    @NotBlank
    private String nome;

    @NotNull
    private TipoCidadeEnum tipoCidade;

    @NotNull
    @NotBlank
    private String telefone;

    public Cliente() {
    }

    public Cliente(int id, @NotNull @NotBlank String nome, @NotNull @NotBlank TipoCidadeEnum tipoCidade, String telefone) {
        this.id = id;
        this.nome = nome;
        this.tipoCidade = tipoCidade;
        this.telefone = telefone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public TipoCidadeEnum getTipoCidade() {
        return tipoCidade;
    }

    public void setTipoCidade(TipoCidadeEnum tipoCidade) {
        this.tipoCidade = tipoCidade;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
}
