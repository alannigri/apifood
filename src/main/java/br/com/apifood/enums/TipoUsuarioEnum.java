package br.com.apifood.enums;

public enum TipoUsuarioEnum {
    CLIENTE,
    RESTAURANTE,
    ADMIN
}
