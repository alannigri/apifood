package br.com.apifood.controllers;

import br.com.apifood.enums.TipoCidadeEnum;
import br.com.apifood.enums.TipoProdutoEnum;
import br.com.apifood.models.Produto;
import br.com.apifood.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

    @Autowired
    private ProdutoService produtoService;

    @GetMapping("/{codigoProduto}")
    public Produto consultarProdutoPorID(@PathVariable int codigoProduto) {
        try {
            Produto produto = produtoService.consultarProdutoPorID(codigoProduto);
            return produto;
        }catch (Exception e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping
    public Iterable<Produto> consultarProdutoPorNomeOuTipoDeProdutoOuCidade
            (@RequestParam(name = "nomeProduto", required = false) String nomeProduto,
             @RequestParam(name = "TipoDeProduto", required = false) TipoProdutoEnum tipoProduto,
             @RequestParam(name = "cidade", required = false) TipoCidadeEnum cidade) {
        if (nomeProduto != null) {
            Iterable<Produto> produtoIterable = produtoService.consultarProdutoPorNome(nomeProduto);
            return produtoIterable;
        } else if (tipoProduto != null) {
            Iterable<Produto> produtoIterable = produtoService.consultarProdutoPorTipoDeProduto(tipoProduto);
            return produtoIterable;
        }
        Iterable<Produto> produtoIterable = produtoService.consultarTodosProdutos();
        return produtoIterable;
    }

    @GetMapping("/{id}/status")
    public boolean consultarSeProdutoEstaDisponivel(@PathVariable int id) {
        try {
            boolean status = produtoService.consultarSeProdutoEstaDisponivel(id);
            return status;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
