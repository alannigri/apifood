package br.com.apifood.controllers;

import br.com.apifood.configurations.ConfiguracaoDeSeguranca;
import br.com.apifood.enums.TipoCategoriaEnum;
import br.com.apifood.enums.TipoCidadeEnum;
import br.com.apifood.enums.TipoProdutoEnum;
import br.com.apifood.models.Produto;
import br.com.apifood.models.Restaurante;
import br.com.apifood.security.JWTUtil;
import br.com.apifood.services.LoginUsuarioService;
import br.com.apifood.services.ProdutoService;
import br.com.apifood.services.RestauranteService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@WebMvcTest(RestauranteController.class)
public class RestauranteControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private JWTUtil jwtUtil;

    @MockBean
    private LoginUsuarioService loginUsuarioService;

    @MockBean
    private RestauranteService restauranteService;

    @MockBean
    private ProdutoService produtoService;

    Restaurante restaurante;
    Produto produto;
    List<Produto> produtos;

    @BeforeEach
    public void setUp(){
        restaurante = new Restaurante();
        restaurante.setId(1);
        restaurante.setNome("Hamburgueria One");
        restaurante.setTelefone("998688447");
        restaurante.setCategoria(TipoCategoriaEnum.HAMBURGUERIA);
        restaurante.setCidade(TipoCidadeEnum.SAO_PAULO);

        produto = new Produto();
        produto.setId(1);
        produto.setNome("Hamburguer da Casa");
        produto.setDescricao("Hamburgão");
        produto.setPreco(30.00);
        produto.setTipoProduto(TipoProdutoEnum.COMIDA);
        produto.setEstaDisponivel(true);

        produtos = new ArrayList<>();
        produtos.add(produto);

        restaurante.setProdutos(produtos);
    }

    @Test
    public void testarConsultarRestaurantePorIdEncontrado() throws Exception {
        Mockito.when(restauranteService.buscarPorId(Mockito.anyInt())).thenReturn(restaurante);

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeRestaurante = mapper.writeValueAsString(restaurante);

        mockMvc.perform(MockMvcRequestBuilders.get("/restaurantes/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeRestaurante))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarConsultarRestaurantePorNome() throws Exception {
        Iterable<Restaurante> restaurantes = Arrays.asList(restaurante);
        Mockito.when(restauranteService.buscarTodosPorNome(Mockito.anyString())).thenReturn(restaurantes);

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeRestaurante = mapper.writeValueAsString(restaurantes);

        mockMvc.perform(MockMvcRequestBuilders.get("/restaurantes?nome=Hamb")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeRestaurante))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1));
    }

    @Test
    public void testarConsultarRestaurantePorCategoria() throws Exception {
        Iterable<Restaurante> restaurantes = Arrays.asList(restaurante);
        Mockito.when(restauranteService.buscarTodosPorTipoDeRestaurante(Mockito.any())).thenReturn(restaurantes);

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeRestaurante = mapper.writeValueAsString(restaurantes);

        mockMvc.perform(MockMvcRequestBuilders.get("/restaurantes?categoria=HAMBURGUERIA")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeRestaurante))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1));
    }
}
