package br.com.apifood.security;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

public class LoginUsuario implements UserDetails {

    private int id;
    private String email;
    private String senha;


    public LoginUsuario() { }

    public LoginUsuario(int id, String email, String senha) {
        this.id = id;
        this.email = email;
        this.senha = senha;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return this.senha;
    }

    @Override
    public String getUsername() {
        return this.email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true; //falso se estiver expirado. Se precisar expirar, pŕecisa implementar a logica de expiração
    }

    @Override
    public boolean isAccountNonLocked() {
        return true; //implementar logica para verificar se a conta está bloqueada
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true; //veio como false por default
    }

    @Override
    public boolean isEnabled() {
        return true; //veio como false default
    }
}
