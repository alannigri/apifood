package br.com.apifood.services;

import br.com.apifood.enums.TipoCidadeEnum;
import br.com.apifood.models.Cliente;
import br.com.apifood.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente salvarCliente(Cliente cliente){
        Cliente clienteObjeto = clienteRepository.save(cliente);
        return clienteObjeto;
    }

    public Iterable<Cliente> buscarTodosOsClientes(){
        Iterable<Cliente> clientes = clienteRepository.findAll();
        return clientes;
    }

    public Iterable<Cliente>  buscarPorTodosOsTipoCidade(TipoCidadeEnum tipoCidade){
        Iterable<Cliente> clientes = clienteRepository.findAllByTipoCidade(tipoCidade);
        return clientes;
    }

    public Cliente buscarPorId(int id) {
        Optional<Cliente> optionalCliente = clienteRepository.findById(id);
        if (optionalCliente.isPresent()){
            return optionalCliente.get();
        }
        throw new RuntimeException("O cliente não foi encontrado");
    }


    public Cliente atualizarCliente(int id, Cliente cliente){
        if (clienteRepository.existsById(id)){
            cliente.setId(id);
            Cliente clienteObjeto = clienteRepository.save(cliente);
            return clienteObjeto;
        }
        throw new RuntimeException("O cliente não foi encontrado");
    }

    public void deletarCliente(int id){
        if (clienteRepository.existsById(id)){
            clienteRepository.deleteById(id);
        }else{
            throw new RuntimeException("O cliente não foi encontrado");
        }
    }



    //
//    public List<Produto> buscarPorTodosOsId(List<Long> id){
//        Iterable<Produto> produto = produtoRepository.findAllById(id);
//        return (List) produto;

}
