package br.com.apifood.enums;

public enum TipoProdutoEnum {
    BEBIDA,
    COMIDA,
    SOBREMESA;
}
