package br.com.apifood.services;

import br.com.apifood.enums.TipoCategoriaEnum;
import br.com.apifood.enums.TipoCidadeEnum;
import br.com.apifood.models.Restaurante;
import br.com.apifood.repositories.RestauranteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class RestauranteService {

    @Autowired
    private RestauranteRepository restauranteRepository;

    public Restaurante salvarRestaurante(Restaurante restaurante){
        Restaurante restauranteObjeto = restauranteRepository.save(restaurante);
        return restaurante;
    }

    public Restaurante atualizarRestaurante(int id, Restaurante restaurante){
        if (restauranteRepository.existsById(id)){
            restaurante.setId(id);
            Restaurante restauranteObjeto = restauranteRepository.save(restaurante);

            return restauranteObjeto;
        }
        throw new RuntimeException("O restaurante não foi encontrado");
    }

    public void deletarRestaurante(int id){
        if (restauranteRepository.existsById(id)){
            restauranteRepository.deleteById(id);
        }else{
            throw new RuntimeException("O restaurante não foi encontrado");
        }
    }

    public Restaurante buscarPorId(int id){
        Optional<Restaurante> optionalRestaurante = restauranteRepository.findById(id);
        if (optionalRestaurante.isPresent()){

            return optionalRestaurante.get();
        }
        throw new RuntimeException("O restaurante não foi encontrado");
    }
    public Iterable<Restaurante> buscarTodosOsRestaurantes(){
        Iterable<Restaurante> restaurantes = restauranteRepository.findAll();
        return restaurantes;
    }

    public Iterable<Restaurante> buscarTodosPorNome(String nome){
        Iterable<Restaurante> restaurantes = restauranteRepository.findAllByNomeContaining(nome);
        return restaurantes;
    }

    public Iterable<Restaurante> buscarTodosPorTipoDeRestaurante(TipoCategoriaEnum tipoCategoria){
        Iterable<Restaurante> restaurantes = restauranteRepository.findAllByCategoria(tipoCategoria);
        return restaurantes;
    }

    public Iterable<Restaurante> buscarTodosPorCidade(TipoCidadeEnum cidade) {
        Iterable<Restaurante> restaurantes = restauranteRepository.findAllByCidade(cidade);
        return restaurantes;
    }
}
