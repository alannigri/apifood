package br.com.apifood.security;

import br.com.apifood.services.LoginUsuarioService;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FiltroAutorizacao extends BasicAuthenticationFilter {

    private JWTUtil jwtUtil;
    private LoginUsuarioService loginUsuarioService;

    public FiltroAutorizacao(AuthenticationManager authenticationManager, JWTUtil jwtUtil,
                             LoginUsuarioService loginUsuarioService) {
        super(authenticationManager);
        this.jwtUtil = jwtUtil;
        this.loginUsuarioService = loginUsuarioService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        String autorizationHeader = request.getHeader("Authorization");

        if(autorizationHeader != null && autorizationHeader.startsWith("Bearer ")){
            String tokenLimpo = autorizationHeader.substring(7);
            UsernamePasswordAuthenticationToken autenticacao = getAutenticacao(request, tokenLimpo);
            if(autenticacao != null){
                SecurityContextHolder.getContext().setAuthentication(autenticacao);
            }
        }
        chain.doFilter(request, response);

    }

    private UsernamePasswordAuthenticationToken getAutenticacao(HttpServletRequest request, String token){
        //TODO: Avaliar se iremos implementar nivel de usuario (credentials: null)
        if(jwtUtil.tokenValido(token)){
            String username = jwtUtil.getUsername(token);
            UserDetails usuario = loginUsuarioService.loadUserByUsername(username);
            return new UsernamePasswordAuthenticationToken(usuario, null,usuario.getAuthorities());
        }
        return null;
    }
}