package br.com.apifood.enums;

public enum TipoCategoriaEnum {
    ITALIANA,
    JAPONESA,
    PIZZARIA,
    MEXICANA,
    VEGETARIANA,
    HAMBURGUERIA
}
