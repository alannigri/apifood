package br.com.apifood.services;

import br.com.apifood.security.LoginUsuario;
import br.com.apifood.models.Usuario;
import br.com.apifood.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LoginUsuarioService implements UserDetailsService {

    @Autowired
    private UsuarioRepository usuarioRepository;


    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<Usuario> optionalUsuario = usuarioRepository.findByEmail(email);

        if (optionalUsuario.isPresent()) {
            Usuario usuario = optionalUsuario.get();

            LoginUsuario loginUsuario = new LoginUsuario(usuario.getId(), usuario.getEmail(), usuario.getSenha());
            return loginUsuario;
        }
        throw new UsernameNotFoundException("O usuário " + email + " não encontrado.");

    }
}


