package br.com.apifood.repositories;

import br.com.apifood.enums.TipoProdutoEnum;
import br.com.apifood.models.Produto;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProdutoRepository extends CrudRepository<Produto, Integer>{
    List<Produto>findAllByNome(String nome);
    List<Produto>findAllByNomeContaining(String nome);
    List<Produto>findAllByTipoProduto(Enum tipoProduto);

}
