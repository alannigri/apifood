package br.com.apifood.services;


import br.com.apifood.models.Usuario;
import br.com.apifood.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;


    public Usuario salvarUsuario(Usuario usuario) {
//        Optional<Usuario> optionalUsuario = consultarUsuario(usuario.getEmail());
//
//        if (optionalUsuario.isPresent()) {
//            throw new RuntimeException("Usuário já existe no cadastro");
//        }

        String encoder = bCryptPasswordEncoder.encode(usuario.getSenha());

        usuario.setSenha(encoder);
        Usuario usuarioObjeto = usuarioRepository.save(usuario);

        return usuarioObjeto;
    }

    public Optional<Usuario> consultarUsuario(String email){
        Optional<Usuario> optionalUsuario = usuarioRepository.findByEmail(email);
        if(optionalUsuario.isPresent()) {
            return optionalUsuario;
        }else{
            throw new RuntimeException("Usuario não encontrado");
        }
    }

}
