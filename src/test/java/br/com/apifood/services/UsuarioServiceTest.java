package br.com.apifood.services;


import br.com.apifood.enums.TipoUsuarioEnum;
import br.com.apifood.models.Usuario;
import br.com.apifood.repositories.UsuarioRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

@SpringBootTest
public class UsuarioServiceTest {

    @MockBean
    private UsuarioRepository usuarioRepository;

    @Autowired
    private UsuarioService usuarioService;

    Usuario usuario;

    @BeforeEach
    public void setUp() {
        usuario = new Usuario();
        usuario.setId(0);
        usuario.setSenha("aaa111");
        usuario.setEmail("teste1@teste.com");
        usuario.setTipoUsuario(TipoUsuarioEnum.CLIENTE);
    }

    @Test
    public void testarConsultarUsuario() {
        Optional<Usuario> usuarioOptional = Optional.of(usuario);

        Mockito.when(usuarioRepository.findByEmail("teste1@teste.com")).thenReturn(usuarioOptional);

        Optional<Usuario> usuarioRetorno = usuarioService.consultarUsuario("teste1@teste.com");

        Assertions.assertEquals(usuario.getEmail(), usuarioRetorno.get().getEmail());
    }

    @Test
    public void testarConsutarUsuarioNaoExistente() {
        Mockito.when(usuarioRepository.findByEmail("teste@teste.com")).thenThrow(RuntimeException.class);
        Assertions.assertThrows(RuntimeException.class, () -> {
            Optional<Usuario> usuarioOptional = usuarioService.consultarUsuario("teste@teste.com");
        });
    }
}
