package br.com.apifood.enums;

public enum TipoCidadeEnum {
    SAO_PAULO,
    SANTOS,
    CAMPINAS,
    VARGEM_GRANDE_PAULISTA,
    GUARUJA,
    RIO_DE_JANEIRO
}
